'use strict';
const assert = require('assert');
const runsv = require('runsv');

const genServer = require('..');
const REDIS = 'redis://localhost:6379';

function notImplemented(){
	throw new Error('not implemented');
}
function createV3RedisMock(){
	let mock = {
		calls: [],
		createClient(options){
			mock.calls.push(['createClient', options]);
			mock.events = {};
			return {
				type: 'redis-client',
				connect(){
					mock.calls.push(['connect']);
					setImmediate(function(){
						mock.events.ready();
					});
				},
				quit(callback){
					mock.calls.push(['quit']);
					setImmediate(function(){
						callback();
					});

				},
				set(key, value){
					mock.calls.push(['set', key, value]);
				},
				once(event, callback){
					mock.events[event] = callback;
				}
			};
		}
	};
	return mock;
}
function createService(){
	let driver = createV3RedisMock();
	return [genServer.createCallbackService({
		name:'redis',
		driver,
		start(deps, redis, options, callback){
			const client = redis.createClient(options);
			client.connect();
			client.once('ready', () => callback(null, client));
		},
		stop (redis, callback){
			redis.quit(callback);
		}
	})(REDIS), driver];
}
describe('Callback interface',function(){
	it('should work with callback clients', function(done){
		const sv = runsv.create();
		const [service] = createService();
		sv.addService(service);
		sv.start(function(err){
			if(err){
				return done(err);
			}
			const {redis:client} = sv.getClients();
			client.set('k', 1);
			sv.stop(done);
		});
	});
	describe('Prevent double start', function(){
		it('should throw an exception if started more than once', function(done){
			const [service] = createService();
			service.start(null, function(err){
				if(err){
					return done(err);
				}
				service.start(null, function(err){
					assert(err, 'did not callback an error');
					assert.deepStrictEqual(err.message, 'already started: redis');
					return done();
				});
			});
		});
	});
	describe('When stopped before started', function(){
		it('should pass',function(done){
			const [service] = createService();
			service.stop(function(err){
				return done(err);
			});
		});
	});
	describe('Start errored', function(){
		it('should callback that error', function(done){
			let driver = createV3RedisMock();
			let service = genServer.createCallbackService({
				name:'redis',
				driver,
				start(deps, redis, options, callback){
					return callback(new Error('some'));
				},
				stop:notImplemented
			})(REDIS);
			service.start(null, function(err){
				assert.deepStrictEqual(err.message, 'some');
				return done();
			});
		});
	});
	describe('#start(deps, driver, options, callback) not creating a client', function(){
		it('should fail',function(done){
			let driver = createV3RedisMock();
			let service = genServer.createCallbackService({
				name:'redis',
				driver,
				start(deps, redis, options, callback){
					return callback();
				},
				stop:notImplemented
			})(REDIS);
			service.start(null, function(err){
				assert.deepStrictEqual(err.message, 'no client');
				return done();
			});
		});
	});
	describe('#create(options)', function(){
		it('should be able to set up name', function(){
			let driver = createV3RedisMock();
			let service = genServer.createCallbackService({
				name:'first',
				driver,
				start:notImplemented,
				stop:notImplemented
			})({name: 'second'});
			assert.deepStrictEqual(service.name, 'second');
		});
		it('should be able to set up driver', function(done){
			let driver = createV3RedisMock();
			let service = genServer.createCallbackService({
				name:'redis',
				start(deps, driver, options, callback){
					const client = driver.createClient(options);
					return callback(null, client);
				},
				stop:notImplemented
			})({driver});
			service.start(null, function(err){
				const client = service.getClient();
				assert.deepStrictEqual(client.type, 'redis-client');
				return done(err);
			});
		});
	});
});
