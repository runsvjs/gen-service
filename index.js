'use strict';

const assert = require('assert');

function createAsyncService(setup={}){
	const _start = setup.start;
	const _stop = setup.stop;
	assert(setup.name, 'no default name was specified');
	assert(_start, '#start function is required');
	assert(_stop, '#stop function is required');
	return function create(options){
		const name = options && options.name || setup.name;
		const driver = options && options.driver || setup.driver;
		assert(name, 'no name was specified');
		assert(driver, 'no driver was specified');

		let client;
		let ret = {
			name,
			async start(deps){

				assert(driver, 'a driver is required');

				if(client){
					throw new Error('already started: ' + name);
				}

				client = await _start(deps, driver, options);

				if(!client){
					throw new Error('no client');
				}
			},
			async stop(){
				if(!client){
					return new Promise((resolve) => {
						resolve();
					});
				}
				await _stop(client);
				client = null;
			},
			getClient(){
				return client;
			}
		};
		return ret;
	};
}
function createCallbackService(setup={}){
	const _start = setup.start;
	const _stop = setup.stop;
	assert(setup.name, 'no default name was specified');
	assert(_start, '#start function is required');
	assert(_stop, '#stop function is required');
	return function create(options){
		const name = options && options.name || setup.name;
		const driver = options && options.driver || setup.driver;
		assert(name, 'no name was specified');
		assert(driver, 'no driver was specified');
		let client;
		let ret = {
			name,
			start(deps, callback){

				if(client){
					return callback(new Error('already started: ' + name));
				}
				_start(deps, driver, options, function(err, _client){
					if(err){
						return callback(err);
					}
					if(!_client){
						return callback(new Error('no client'));
					}
					client = _client;
					return callback();
				});
			},
			stop(callback){
				if(!client){
					return setImmediate(() => callback());
				}
				_stop(client, function(err){
					client = null;
					return callback(err);
				});
			},
			getClient(){
				return client;
			}
		};
		return ret;
	};
}
exports = module.exports = {
	createAsyncService, 
	createCallbackService
};
